import { AppProps } from 'next/app'
import Link from 'next/link'
import '../styles/index.css'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <nav className="p-4 border-b-2 border-b-gray-500">
        <Link href="https://gurkz.me">
          Lost? Go back home
        </Link>
      </nav>
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
